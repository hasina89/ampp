<?php
	$res  		 = array();

	$page_slug = $post->post_name;

	$args = array(
		'post_type' 		 => 'work',
		);
	if ( is_home() || is_front_page() ){
		$spec_args = array(
			'posts_per_page' => -1,
			'meta_key' 			 => 'show_home',
			'meta_value' 		 => true,
		);
		$args += $spec_args;
		$size  									= 'workimage-home';
	}elseif(  wp_basename('templates/about.php') ){
		$args['posts_per_page'] = 12;
		$size  									= 'isotope-about';
	}elseif( wp_basename('templates/work-page.php') ){
		$args['posts_per_page'] = -1;
		$size  									= 'isotope-work';
	}

	$q = new WP_Query( $args );

	if ( $q->have_posts() ){
		$res = $q->posts;		
	}

	wp_reset_postdata();
