<?php
	$contacts = get_field('liste_des_contacts');
	
	if ( is_array( $contacts ) ):
		foreach ( $contacts as $key => $contact ):
			$ID = to_slug($contact['nom_complet']);
?>

		<div class="profil profil<?= $key+1 ?>">
			<h2 data-aos="fade-up" data-aos-duration="800"><?= make_2lines( $contact['nom_complet']) ?>.</h2>
			<div class="sousProfil">
				<div class="post" data-aos="fade-up" data-aos-duration="1000">
					<p><?= $contact['poste_occupe'] ?></p>
					<p><a href="mailto:<?= $contact['adresse_e-mail'] ?>" title="<?= $contact['adresse_e-mail'] ?>"><?= $contact['adresse_e-mail'] ?></a></p>
				</div>
				<?php if ( $contact['profil_linkedin'] ): ?>
					<div class="postSocial" data-aos="fade-up" data-aos-duration="1200">
						<img src="<?= IMG_URL ?>linkden.svg" alt="">
						<p><a href="<?= $contact['profil_linkedin'] ?>" target="_blank">Check my<br> LinkedIn profile</a></p>
					</div>
				<?php endif; ?>
			</div>
			<a href="#contact-<?= $ID ?>" class="link fancybox" data-fancybox="" title="Contact Me" data-aos="fade-up" data-aos-duration="1600"><span>CONTACT ME</span></a>
			<div class="popup-contact" id="contact-<?= $ID ?>" style="display: none">
			<?php echo do_shortcode( '[contact-form-7 id="216" title="Contact Agent" agent="'. $contact['adresse_e-mail'] .'" admin_cc="'.get_field('adresse_email_admininstrateur_en_cc').'"]' ); ?>
			</div>
		</div>
<?php 
		endforeach;
	endif;
?>  
