<?php 

function more_crypt_ajax(){

    $args = array(
        'suppress_filters' => true,
        'post_type'        => 'crypt',
        'posts_per_page'   => 999,
        'offset'           => 4,
    );

    $loop = new WP_Query($args);

    $out = '';

    if ($loop -> have_posts()) :  while ($loop -> have_posts()) : $loop -> the_post();
        $bg_color = get_field('couleur_en_hover_au_survol') ? get_field('couleur_en_hover_au_survol') : '#ffffff';

        $size = 'isotope-work';
        $illustration = get_field('image_crypt' );
        $img = "<img src='". IMG_URL ."placeholder.jpg' alt='". get_the_title() ."' width='670' height='685' />";
        if ( $illustration ){
            $img = wp_get_attachment_image( $illustration, $size, array( 'alt' => get_the_title() ) );
        }

         $out .= '<a href="javascript: void(0)" class="element-item">
                    <figure>
                        <span class="color" style="background:'. $bg_color.'cc;"></span>
                        '.$img.'
                    </figure>
                </a>';

    endwhile;
    endif;
    wp_reset_postdata();

    echo $out;

    die();
}

add_action('wp_ajax_more_crypt_ajax', 'more_crypt_ajax');
add_action('wp_ajax_nopriv_more_crypt_ajax', 'more_crypt_ajax');
