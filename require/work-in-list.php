<?php
require( THEME_DIR."/require/work-loop.php" );

if ( $res ):
?>
  <div id="vue" class="listItem gridWork" data-aos="fade-up"  data-aos-duration="1200">
<?php
  foreach ( $res as $re ):
    $id = $re->ID;
    $client = get_field('le_client', $id); 
      if ( is_array( $client) ){
        $client = $client[0]->post_title;
      }else{
        $client = '';
      }
    $realisation = get_the_title($id);
    $size = 'isotope-work';

    $illustration = get_field('lillustration', $id );
    $img = "<img src='". IMG_URL ."placeholder.jpg' alt='". get_the_title($id) ."' width='670' height='685' />";
    if ( $illustration ){
      $img = wp_get_attachment_image( $illustration, $size, array( 'alt' => get_the_title($id) ) );
    }
    $bg_color = get_field('couleur_en_hover_survol', $id) ? get_field('couleur_en_hover_survol', $id) : '#ff23ee';
    $classname  = get_hashtags( $id );
    $url_parms = str_replace(' ', '_', $classname);
?>
  	<a href="<?= get_permalink( $id ) ?>" 
      class="element-item <?= $classname ?>">
      <figure>
        <span class="color" style="background: <?= $bg_color.'cc' ?>;"></span>
        <?= $img ?>
        <div class="txt-hover">
          <h2><?= $realisation ?></h2>
          <span><?= "Client : ". $client ?></span>
        </div>
      </figure>
    </a>

<?php
  endforeach;
?>
  </div>
<?php
endif;
