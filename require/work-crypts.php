<?php
	$res  		 = array();

	$page_slug = $post->post_name;

	$args = array(
		'post_type' 		 => 'crypt',
		'posts_per_page' => 4,
		);

	$q = new WP_Query( $args );

	if ( $q->have_posts() ){
		while ( $q->have_posts() ){
			$q->the_post();
			$bg_color = get_field('couleur_en_hover_au_survol') ? get_field('couleur_en_hover_au_survol') : '#ff23ee';
    	$size = 'isotope-work';
			$illustration = get_field('image_crypt' );
			$img = "<img src='". IMG_URL ."placeholder.jpg' alt='". get_the_title() ."' width='670' height='685' />";
			if ( $illustration ){
				$img = wp_get_attachment_image( $illustration, 'full', array( 'alt' => get_the_title() ) );
			}
	?>
		<a href="<?php the_permalink() ?>" title="" class="element-item">
			<figure>
				<span class="color" style="background: <?= $bg_color.'cc' ?>;"></span>
				<?= $img ?>
			</figure>
		</a>	
	<?php
		}	
	}

	wp_reset_postdata();

	?>