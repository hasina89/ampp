<?php
	$refs = array();
	$args = array(
		'post_type' 		 => 'reference',
		'posts_per_page' => -1,
		'orderby' 			 => 'title',
		'order' 				 => 'ASC',
		);

	$qs = query_posts( $args );

	foreach ( $qs as $q ){
		$refs[] = $q;
	}
