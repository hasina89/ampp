<?php 
if ( $res ):
?>
  <div id="vue" class="listItem grid" data-aos="fade-up"  data-aos-duration="1600">
<?php
  foreach ( $res as $re ):
    $id = $re->ID;
    $client = get_field('le_client', $id); 
      if ( is_array( $client) ){
        $client = $client[0]->post_title;
      }else{
        $client = '';
      }
    $realisation = get_the_title($id);

    $illustration = get_field('lillustration', $id );
    $img = "<img src='". IMG_URL ."placeholder.jpg' alt='". get_the_title($id) ."' width='310' height='317' />";
    if ( $illustration ){
      $img = wp_get_attachment_image( $illustration, 'isotope-work', array( 'alt' => get_the_title($id) ) );
    }
    $bg_color = get_field('couleur_en_hover_survol', $id) ? get_field('couleur_en_hover_survol', $id) : '#ff23ee';
    $classname  = get_hashtags( $id );

?>
  	<a href="<?= get_permalink( $id ) ?>" 
      class="element-item <?= $classname ?>">
      <span class="color" style="background: <?= $bg_color.'cc' ?>;"></span>
      <?= $img ?>
      <div class="txt-hover">
        <h2><?= $realisation ?></h2>
        <span><?= "Client : ". $client ?></span>
      </div>
    </a>


<?php
  endforeach;
?>
  </div>
<?php
endif;
