<?php
	$video = get_field('uploader_une_video');
	if ( $video ){
		$type = $video['mime_type'];
		$src = $video['url'];
		if ( get_field( 'thumbnail' ) ){
			$thumbnail = wp_get_attachment_image_src( get_field( 'thumbnail' ), 'thumbnail-video', ["alt"=>"AMPP"] );
			$thumbnail = $thumbnail[0];
		}
	?>
		<section   class="section section1" data-section="video-about">
      	<div class="wrapper">
            <div class="blcVideo" data-aos="fade-up"  data-aos-duration="800">
            	 <video class="video" id="video" preload="metadata"  poster="<?= $thumbnail ?>">
                    <source src="<?= $src ?>" type="<?= $type ?>">
                </video>
                <div class="button-video">
                  <span class="play"><img src="<?= IMG_URL ?>icon-play.png"></span>
                  <span class="pause"><img src="<?= IMG_URL ?>icon-pause.png"></span>
                </div>
            </div>
          </div>
      </section>

	<?php
	}
