<?php


/* enqueue style & scipts */
function load_css_js_front() {
	/* deregister jquery core */
	if (!is_admin()){
		wp_deregister_script( 'jquery' );
		wp_register_script( 'jquery', JS_URL . 'jquery.min.js', array(), true, false, true );
		wp_enqueue_script( 'jquery', false, array(), false, true );
	}	
	wp_register_script( 'slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), true, false, true );
	wp_register_script( 'easing', JS_URL . 'easing.js', array(), true, false, true );
	wp_register_script( 'wow', JS_URL . 'wow.js', array(), true, false, true );
	wp_register_script( 'scrolloverflow', JS_URL . 'scrolloverflow.min.js', array(), true, false, true );
	wp_register_script( 'fullpage', JS_URL . 'fullpage.js', array(), true, false, true );
	wp_register_script( 'aos', JS_URL . 'aos.js', array(), true, false, true );
	wp_register_script( 'isotope', JS_URL . 'isotope.pkgd.js', array(), true, false, true );
	wp_register_script( 'fancyb', JS_URL . 'fancybox/jquery.fancybox.js', array(), true, false, true );	
	wp_register_script( 'custom', JS_URL . 'custom.js', array(), true, false, true );
	wp_register_script( 'wow-page', JS_URL . 'wow-page.js', array(), true, false, true );
	wp_register_script( 'butter', JS_URL . 'butter.js', array(), true, false, true );
	wp_register_script( 'scrollify', JS_URL . 'jquery.scrollify.js', array(), true, false, true );

	wp_enqueue_script( 'slick', false, array(), false, true );
	wp_enqueue_script( 'easing', false, array(), false, true );
	wp_enqueue_script( 'wow', false, array(), false, true );
	wp_enqueue_script( 'scrolloverflow', false, array(), false, true );
	wp_enqueue_script( 'fullpage', false, array(), false, true );
	wp_enqueue_script( 'aos', false, array(), false, true );
	wp_enqueue_script( 'isotope', false, array(), false, true );
	wp_enqueue_script( 'fancyb', false, array(), false, true );
	wp_enqueue_script( 'butter', false, array(), false, true );
	wp_enqueue_script( 'scrollify', false, array(), false, true );
	wp_enqueue_script( 'custom', false, array(), false, true );
	wp_enqueue_script( 'wow-page', false, array(), false, true );
	

	wp_register_script( 'wp_js', JS_URL . 'wp-js.js', array(), true, false, true );
	wp_enqueue_script( 'wp_js', false, array(), false, true );
	wp_localize_script('wp_js', 'ajax_works', array( 
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'nocrypts'=> 'Anything found',
	) );

	wp_enqueue_style( 'animate', CSS_URL . 'animate.css', array(), time(), 'all' );
	wp_enqueue_style( 'slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array(), time(), 'all' );
	wp_enqueue_style( 'fullpage', CSS_URL . 'fullpage.css', array(), time(), 'all' );
	wp_enqueue_style( 'aos', CSS_URL . 'aos.css', array(), time(), 'all' );
	wp_enqueue_style( 'fancy', JS_URL . 'fancybox/jquery.fancybox.css', array(), time(), 'all' );
	wp_enqueue_style( 'reset', CSS_URL . 'reset.css', array(), time(), 'all' );
	wp_enqueue_style( 'butter-css', CSS_URL . 'butter.css', array(), time(), 'all' );
	
}
add_action( 'wp_enqueue_scripts', 'load_css_js_front' );

// image custom size
function ampp_theme_setup() {
    add_image_size( 'workimage-home', 1290, 690, true );
    add_image_size( 'rightimage-home', 625, 455, true );
    add_image_size( 'rightimage-about', 490, 615, true );
    add_image_size( 'isotope-about', 310, 317, true );
    add_image_size( 'isotope-work', 670, 685, true );
    add_image_size( 'footer-logo', 122, 43, true );
    add_image_size( 'thumbnail-video', 1365, 795, true );

}
add_action( 'after_setup_theme', 'ampp_theme_setup' );

function register_my_menus() {
  register_nav_menus(
    array(
      'top-menu' => __( 'Menu principal' ),
      'bottom-menu' => __( 'Menu footer' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );

function remove_comment_menu(){
	remove_menu_page( 'edit-comments.php' );
	remove_menu_page( 'edit.php' );
}
add_action( 'admin_menu', 'remove_comment_menu' );

function ampp_customize_register($wp_customize){

	// Add settings and control for logo upload
	$wp_customize->add_setting("upload_logo", array(
		// "type" => "theme_mod", // or 'option'
		"capability" => "edit_theme_options",
		"default" => IMG_URL . "logo.png",
		"transport" => "refresh",
		'sanitize_callback' => '',
  		'sanitize_js_callback' => '' // Basically to_json.
	));
	$wp_customize->add_control( new WP_Customize_Media_Control(
		$wp_customize, 'upload_logo', 
		array( // setting id
			'label'    => __( 'Upload Logo', 'ampp' ),
			'section'  => 'title_tagline',
			'priority' => 1,
		)
	) );

	// add twetter account settings 
	$wp_customize->add_setting("tweeter_profile", array(
		// "type" => "theme_mod", // or 'option'
		"capability" => "edit_theme_options",
		"default" => "TwitterDev",
		"transport" => "refresh",
		'sanitize_callback' => '',
  		'sanitize_js_callback' => '' // Basically to_json.
	));
	$wp_customize->add_control( 'tweeter_profile', array( // setting id
	'label'    => __( 'Profil Tweeter du site AMPP', 'ampp' ),
	'section'  => 'title_tagline', // section id
	'type'     => 'text',
	'priority' => 5,
) );

	$wp_customize->add_panel( 'panel_id', array(
    'priority'       => 160,
    'capability'     => 'edit_theme_options',
    'theme_supports' => '',
    'title'          => 'Options du footer',
    'description'    => '',
) );

/*
	Structure of footer: 4 cols 
	First column footer ==> logo
	Second colun footer ==> Title + email + phone
	Third column fotter ==> Title + email
	Forth column footer ==> page menu 
*/

//1st col
$wp_customize->add_section( 'footer-options', array(
    'title' => __( 'Logo footer', 'ampp' ),
    'priority' => 1,
    'panel' => 'panel_id',
) );
$wp_customize->add_setting("footer_logo", array(
	"type" => "theme_mod", // or 'option'
	"capability" => "edit_theme_options",
	"default" => IMG_URL . "logo.png",
	"transport" => "refresh",
	'sanitize_callback' => '',
		'sanitize_js_callback' => '' // Basically to_json.
));
$wp_customize->add_control(new WP_Customize_Media_Control(
	$wp_customize, 'footer_logo', 
	array( // setting id
		'label'    => __( 'Logo Footer', 'ampp' ),
		'section'  => 'footer-options',
		'priority' => 1,
	)
) );

// 2nd col 
$wp_customize->add_section( 'second-col-footer', array(
    'title' => __( 'Deuxième colonne', 'ampp' ),
    'priority' => 9,
    'panel' => 'panel_id',
) );
$wp_customize->add_setting("title_col2", array(
	"type" => "theme_mod", // or 'option'
	"capability" => "edit_theme_options",
	"default" => "NEW BUSINESS INQUIRIES",
	"transport" => "refresh",
));
$wp_customize->add_control( 'title_col2', array( // setting id
	'label'    => __( 'Titre dans la deuxième colonne', 'ampp' ),
	'section'  => 'second-col-footer', // section id
	'type'     => 'text',
	'priority' => 1,
) );
$wp_customize->add_setting("email_col2", array(
	"type" => "theme_mod", // or 'option'
	"capability" => "edit_theme_options",
	"default" => "hello@ampp.be",
	"transport" => "refresh",
	'sanitize_callback' => 'ampp_sanitize_email',
));
$wp_customize->add_control( 'email_col2', array( // setting id
	'label'    => __( 'Email dans la deuxième colonne', 'ampp' ),
	'section'  => 'second-col-footer', // section id
	'type'     => 'email',
	'priority' => 2,
) );
$wp_customize->add_setting("phone_col2", array(
	"type" => "theme_mod", // or 'option'
	"capability" => "edit_theme_options",
	"default" => "+32 (0)2 524 23 22",
	"transport" => "refresh",
));
$wp_customize->add_control( 'phone_col2', array( // setting id
	'label'    => __( 'Téléphone dans la deuxième colonne', 'ampp' ),
	'section'  => 'second-col-footer', // section id
	'type'     => 'text',
	'priority' => 2,
) );

// 3rd col 
$wp_customize->add_section( 'third-col-footer', array(
    'title' => __( 'Troisième colonne', 'ampp' ),
    'priority' => 11,
    'panel' => 'panel_id',
) );
$wp_customize->add_setting("title_col3", array(
	"type" => "theme_mod", // or 'option'
	"capability" => "edit_theme_options",
	"default" => "JOBS & INTERNSHIPS",
	"transport" => "refresh",
));
$wp_customize->add_control( 'title_col3', array( // setting id
	'label'    => __( 'Titre dans la Troisième colonne', 'ampp' ),
	'section'  => 'third-col-footer', // section id
	'type'     => 'text',
	'priority' => 1,
) );
$wp_customize->add_setting("email_col3", array(
	"type" => "theme_mod", // or 'option'
	"capability" => "edit_theme_options",
	"default" => "work@ampp.be",
	"transport" => "refresh",
	'sanitize_callback' => 'ampp_sanitize_email',
));
$wp_customize->add_control( 'email_col3', array( // setting id
	'label'    => __( 'Email dans la troisième colonne', 'ampp' ),
	'section'  => 'third-col-footer', // section id
	'type'     => 'email',
	'priority' => 2,
) );

// 4th col
// menu bottom

function ampp_sanitize_email( $email, $setting ) {
	return ( is_email($email) ? $email : $setting->default );
}

}
add_action( 'customize_register', 'ampp_customize_register' );

if ( !function_exists( 'get_theme_mod' ) ) { 
    require_once ABSPATH . WPINC . '/theme.php'; 
} 

add_action('pre_get_posts', 'ampp_custom_order_by');
function ampp_custom_order_by( $query ){
	if( ! is_admin() || ! $query->is_main_query() ) {
    return;
  }

  if ( 'order_by_realisation_name' === $query->get( 'orderby') ){
  	$query->set( 'orderby', 'title' );
  }

  if ( 'order_by_realisation_client' === $query->get( 'orderby') ){
  	$query->set( 'orderby', 'meta_value' );
    $query->set( 'meta_key', 'nom_du_client' );
  }

}

//Reorder submnu crypt et ralisaiton 
add_filter( 'custom_menu_order', 'ampp_reorder_submenu' );

function ampp_reorder_submenu($menu_order){
	global $submenu;
	
	$arr = array();
  $arr[] = $submenu['edit.php?post_type=work'][5];
  $arr[] = $submenu['edit.php?post_type=work'][15];
  $arr[] = $submenu['edit.php?post_type=work'][16];

  $submenu['edit.php?post_type=work'] = $arr;

  return $menu_order;
}

add_action( 'save_post', 'realisation_name_as_title' ,10, 3 );

function realisation_name_as_title( $post_id, $post , $update ){
	if( ! $update ) {
    	return;
	}

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
    return;
  
  if ( $post->post_type != 'work')
  	return;

  // verify post is not a revision
  if ( ! wp_is_post_revision( $post_id ) ) {

  		// unhook this function to prevent infinite looping
      remove_action( 'save_post', 'realisation_name_as_title' );

  		$client = get_field('le_client', $post_id); 
      if ( is_array( $client) ){
        $client = $client[0]->post_title;
        // update the post slug
	      wp_update_post( array(
	          'ID' => $post_id,
	          'post_name' => sanitize_title( $client ),
	      ));
      }else{
        $client = '';
      }
      // re-hook this function
      add_action( 'save_post', 'realisation_name_as_title' );

  }
  
  
}
