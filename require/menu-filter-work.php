<?php
	$terms = get_terms( array(
	    'taxonomy' => 'hashtag',
	    'hide_empty' => false,
	) );

	$attr = '';

	if (  is_page('about') )
		$attr = ' data-aos="fade-up" data-aos-duration="1000"';
?>
	<ul class="list-filter"<?= $attr ?> data-aos="fade-up"  data-aos-duration="1000">
		<?php foreach ( $terms as $term ): ?>
			<li data-filter=".<?= $term->slug ?>"><span>#<?= $term->name; ?></span></li>
		<?php endforeach; ?>
	</ul>
