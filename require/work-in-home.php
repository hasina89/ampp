<?php
	if ( $res ):
		foreach ( $res as $re ): 
			$id = $re->ID;
			$client = get_field('le_client', $id); 
			if ( is_array( $client) ){
				$client = $client[0]->post_title;
			}else{
				$client = '';
			}
			$realisation = get_the_title($id);

			$illustration = get_field('lillustration', $id ); 
			$img = "<img src='". IMG_URL ."placeholder.jpg' alt='". get_the_title($id) ."' width='1290' height='690' />";
			if ( $illustration ){
				$img = wp_get_attachment_image( $illustration, 'workimage-home', array( 'alt' => get_the_title($id) ) );
			}
			$bg_color = get_field('couleur_en_hover_survol', $id) ? get_field('couleur_en_hover_survol', $id) : '#ff23ee';

?>
		<section class="section section2 sec-home  sec2-home sec-produit-home <?= to_slug( get_the_title($id) ) ?>" data-section="<?= to_slug( get_the_title($id) ) ?>">
		    <div class="blc-img1">
	            <div class="blc-img rouge">
	                <div class="img element-item" data-aos="fade-up"  data-aos-duration="600">
	                	<a href="<?= get_permalink( $id ) ?>">
	                		<span class="color" style="background: <?= $bg_color.'cc' ?>;"></span>
	                    <?= $img ?>
	                    <div class="txt-hover">
	                        <h2><?= $realisation ?></h2>
	                        <span><?= "Client : ". $client ?></span>
	                    </div>
	                  </a>
	                </div>    
	            </div>
		    </div>
		</section>
	<?php 
		endforeach;
	endif; 
