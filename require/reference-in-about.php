<?php
	if ( $refs ):
?>
		<ul class="list-ref">
<?php
		foreach ( $refs as $ref ):
			$reference = $ref->post_title;
			$link 		 = get_field('lien_vers_cette_reference', $ref->ID);
?>
			<li>
				<a href="<?= $link ?>" target="_blank" title="<?= $reference ?>">
					<?= $reference ?>
				</a>
			</li>
<?php
		endforeach;
	endif;
	