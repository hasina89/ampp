// Preloader //
(function($) {
    function handlePreloader() {
        if ($('#preloder').length) {
            $('#preloder').delay(200).fadeOut(500);
        }
    }
    $(window).on('load', function() {
        handlePreloader();
    });
})(window.jQuery);

$(document).ready(function() {

    // MENU MOBILE //
    $(".wrapMenuMobile").click(function() {
        $(this).toggleClass('active');
        $(".menuMobile").toggleClass('active');
        $(".menu ul").toggleClass('active');
        $(".headerTop").toggleClass('active');
        $(".sub").css('display', 'none');
        $(".menu li i").removeClass('active');
    });
    $(".menu li i").click(function() {
        $(this).toggleClass('active');
        $(".menu").find('.sub').slideUp();
        if ($(this).hasClass("active")) {
            $(".menu li i").removeClass('active')
            $(this).next().slideToggle();
            $(this).toggleClass('active');
        }
    });

    if( $('#bg_slide:visible').length > 0 ){
        $('#bg_slide').slick({
            dots: false,
            autoplaySpeed: 6000,
            speed: 980,
            cssEase: 'linear',
            arrows: true,
            fade: false,
            autoplay: true,
            pauseOnHover: false,
            centerPadding: '0px',
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            prevArrow: '<button class="btnArrow prevArrow"><span class="dashicons dashicons-arrow-left-alt2"></span></button>',
            nextArrow: '<button class="btnArrow nextArrow"><span class="dashicons dashicons-arrow-right-alt2"></span></button>',
            responsive: [{
                breakpoint: 760,
                settings: {
                    
                }
            }]
        });
    }

    // PLAY VIDEO
    $('.button-video').click(function() {
        $(this).toggleClass('hide pause')
        console.log('Tratra');
        var vid = $('#video');
        vid[0].paused ? vid[0].play() : vid[0].pause();
    });

    // SCROLL //
    $(".scroll").click(function() {
        var c = $(this).attr("href");
        $('html, body').animate({
            scrollTop: $(c).offset().top - 50
        }, 800, "linear");
        return false;
    });

});


// 	AOS ANIMATION SCROLL //
AOS.init();

// ISOTOP FILTER
// init Isotope
// $(window).on('load', function() {
//     $(".page-template-about .list-filter li").first().trigger('click');
//     $(".page-template-work-page .list-filter li").first().trigger('click');
// });

var $grid = $(".grid").isotope({
    itemSelector: ".element-item",
    layoutMode: "fitRows",
    filter: '*',
});

var $gridWork = $(".gridWork").isotope({
    itemSelector: ".element-item",
    layoutMode: "fitRows",
    filter: '*',
});

// filter functions
var filterFns = {};

// bind filter button click
$(".list-filter").on("click", "li", function() {

    var filterValue = $(this).attr("data-filter");
    if( $(this).hasClass('is-checked')){ 
        $grid.isotope({
            filter: '*'
        });

        $gridWork.isotope({
            filter: '*'
        });

    }
    else{ 
       $grid.isotope({
            filter: filterValue
        });

        $gridWork.isotope({
            filter: filterValue
        }); 

    }
    

    // $.fn.fullpage.reBuild();
    butter.init({cancelOnTouch: true});
    AOS.refresh();

});

// change is-checked class on buttons
$(".list-filter").each(function(i, buttonGroup) {
    var $buttonGroup = $(buttonGroup);
    $buttonGroup.on("click", "li", function() {
        $buttonGroup.find(".is-checked").removeClass("is-checked");
        $(this).addClass("is-checked");
    });
});


// $(window).scroll(function() {
//     var deco1 = $(document).scrollTop() * 0.7 + 2;
//     $(".h1").css("background-position-y", deco1);

// });

$("a.fancybox").fancybox();

$(".scrollTop").click(function() {
     $('html, body').animate({
      scrollTop: $("#first").offset().top
    }, 1000)
});

$(".list-filter li span").click(function() {
     $('html, body').animate({
      scrollTop: $("#vue").offset().top
    }, 1000)
});


$(document).ajaxComplete(function() {
 butter.init({cancelOnTouch: true});
    
});



// butter scroll
butter.init({cancelOnTouch: true});

$(function() {
    if($('#butter').hasClass('home')){
      w=$(window).width();
      if(w>1023){
         $.scrollify({
            section : ".sec-home",
            sectionName: false,
            easing: "easeOutExpo",
            interstitialSection : ".section0,.sec1-home,.sec2-home,.sec3-home,.sec-foot",
            standardScrollElements: "",
            setHeights: true,
            overflowScroll: true,
          });
      }
      else{
        $.scrollify.destroy();
      }
     
    }
});

