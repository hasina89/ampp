(function($){

var ppp = 4; // Post per page
var pageNumber = 1;


function load_posts(){
    pageNumber++;
    // var str = '&pageNumber=' + pageNumber + '&ppp=' + ppp + '&action=more_crypt_ajax';
    $.ajax({
        type: "POST",
        dataType: "html",
        url: ajax_works.ajaxurl,
        data: {
        	'pageNumber' : pageNumber,
        	'ppp': ppp,
        	'action': 'more_crypt_ajax'
        },
        success: function(data){
            var $data = $(data);
            if($data.length){
                $("#crypt-container").append($data);
                $("#load-more").attr("disabled",false);
            } else{
                $("#load-more").attr("disabled",true);
            }
        }
        // error : function(jqXHR, textStatus, errorThrown) {
        //     console.log(jqXHR + " :: " + textStatus + " :: " + errorThrown);
        // }

    });
    return false;
}

$("#load-more").on("click",function(){
    load_posts();
    $(this).remove();
});

})(jQuery)