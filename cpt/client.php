<?php
	add_action( 'init', 'client_cpt', 0 );
	
	function client_cpt() {

	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Clients', 'ampp'),
		// Le nom au singulier
		'singular_name'       => _x( 'Client', 'ampp'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Clients'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Toutes les Clients'),
		'view_item'           => __( 'Voir le client'),
		'add_new_item'        => __( 'Ajouter un Client'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer'),
		'update_item'         => __( 'Modifier'),
		'search_items'        => __( 'Rechercher'),
		'not_found'           => __( 'Aucun Client'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	
	$args = array(
		'label'         => __( 'Clients'),
		'description'   => __( 'Tous les Clients'),
		'labels'        => $labels,
		'menu_icon'     => 'dashicons-admin-users',
		'supports'      => array( 'title', 'custom-fields', ),
		'show_in_rest' 	=> true,
		'hierarchical'  => false,
		'public'        => true,
		'has_archive'   => true,
		'rewrite'			  => array( 'slug' => 'client'),

	);
	
	register_post_type( 'client', $args );
}