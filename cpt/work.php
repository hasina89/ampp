<?php
	add_action( 'init', 'work_cpt', 0 );
	add_action( 'init', 'work_cpt_taxonomy', 0 );
	add_filter( 'manage_work_posts_columns', 'ampp_columns_managing' );
	add_action( 'manage_work_posts_custom_column', 'ampp_custom_column', 10, 2);
	add_filter( 'manage_edit-work_sortable_columns', 'ampp_sortable_columns');

	function work_cpt() {

	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Réalisations', 'Post Type General Name'),
		// Le nom au singulier
		'singular_name'       => _x( 'Réalisation', 'Post Type Singular Name'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Réalisations'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Toutes les réalisations'),
		'view_item'           => __( 'Voir la réalisation'),
		'add_new_item'        => __( 'Ajouter une réalisation'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer'),
		'update_item'         => __( 'Modifier'),
		'search_items'        => __( 'Rechercher'),
		'not_found'           => __( 'Aucune rélisation'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	
	$args = array(
		'label'         => __( 'Works'),
		'description'   => __( 'Toutes les réalisations'),
		'labels'        => $labels,
		'menu_icon'     => 'dashicons-hammer',
		'supports'      => array( 'title', 'custom-fields', 'editor', 'dark-editor-style', ),
		'show_in_rest' 	=> true,
		'hierarchical'  => false,
		'public'        => true,
		'has_archive'   => true,
		'rewrite'			  => array( 'slug' => 'work'),

	);
	
	register_post_type( 'work', $args );
}

function work_cpt_taxonomy(){
	$labels = array(
		'name' => 'Capability',
		'singular_name' 		=> 'Capability',
		'search_items' 			=>  __( 'Rechercher' ),
    'all_items' 				=> __( 'Tous les Capabilities' ),
    'parent_item' 			=> __( 'Parent' ),
    'parent_item_colon' => __( 'Parent :' ),
    'edit_item' 				=> __( 'Editer' ), 
    'update_item' 			=> __( 'Mettre à jour' ),
    'add_new_item' 			=> __( 'Ajouter un nouveau Capability' ),
    'new_item_name' 		=> __( 'Nouveau nom de Capability' ),
    'menu_name'					 => __( 'Capability' ),
		);

	register_taxonomy( 'hashtag', array('work','crypt'), array(
    'hierarchical' 			=> true,
    'labels'						=> $labels,
    'show_ui' 					=> true,
    'show_in_rest' 			=> true,
    'show_admin_column' => true,
    'query_var' 				=> true,
    'rewrite' 					=> array( 'slug' => 'hashtag' ),
  ));
}

function ampp_columns_managing( $columns ){
	$columns = array(
		'cb' 							 => $columns['cb'],
		'illustration'  	 => 'Illustration',
		'realisation_name' => 'Nom du produit',
		'realisation_client' => 'Nom du client',
		'hashtag' 				 => 'Hashtag',
		);
	return $columns;
}

function ampp_custom_column( $column, $post_id ){

	if ( 'illustration' == $column ){
		$illustration = get_field('lillustration', $post_id );

		if ( !$illustration ){
			echo "<img src='". IMG_URL ."placeholder.jpg' alt='". get_the_title($post_id) ."' width='80' height='80' />";
		}else{
			echo wp_get_attachment_image( $illustration, array(80,80) );
		}
	}

	if ( 'realisation_name' == $column ){		
		echo "<strong><a href='".get_edit_post_link()."' title=''>".get_the_title( $post_id )."</a></strong>";		
	}

	if ( 'realisation_client' == $column ){
		$name = get_field('le_client', $post_id );
		if ( !is_array($name) ){
			echo "<strong><a href='".get_edit_post_link()."' title=''>Undefined client</a></strong>";
		}else{
			echo "<strong><a href='".get_edit_post_link()."' title=''>".$name[0]->post_title."</a></strong>";
		}

	}

	if ( 'hashtag' == $column ){
		$tax = "hashtag";
		$terms = get_the_terms( $post_id, $tax );
		if ( is_array( $terms) ){
			foreach ($terms as $k => $term ){
				echo "#".$term->name;
				if ( $k < count($terms) - 1 ){
					echo ", ";
				}
			}
		}
	}
}

function ampp_sortable_columns( $col ){
	$col['realisation_name'] = 'order_by_realisation_name';
	$col['realisation_client'] = 'order_by_realisation_client';

	return $col;
}

