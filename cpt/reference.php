<?php
	add_action( 'init', 'reference_cpt', 0 );
	
	function reference_cpt() {

	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Références', 'ampp'),
		// Le nom au singulier
		'singular_name'       => _x( 'Référence', 'ampp'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Références'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Toutes les Références'),
		'view_item'           => __( 'Voir la Référence'),
		'add_new_item'        => __( 'Ajouter une Référence'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer'),
		'update_item'         => __( 'Modifier'),
		'search_items'        => __( 'Rechercher'),
		'not_found'           => __( 'Aucune Référence'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	
	$args = array(
		'label'         => __( 'Références'),
		'description'   => __( 'Toutes les Références'),
		'labels'        => $labels,
		'menu_icon'     => 'dashicons-awards',
		'supports'      => array( 'title', 'custom-fields', ),
		'show_in_rest' 	=> true,
		'hierarchical'  => false,
		'public'        => true,
		'has_archive'   => true,
		'rewrite'			  => array( 'slug' => 'reference'),

	);
	
	register_post_type( 'reference', $args );
}