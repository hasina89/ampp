<?php
	add_action( 'init', 'crypt_cpt', 0 );
	add_filter( 'manage_crypt_posts_columns', 'ampp_columns_managing_crypt' );
	add_action( 'manage_crypt_posts_custom_column', 'ampp_custom_column_crypt', 10, 2);
	
	function crypt_cpt() {

	$labels = array(
		// Le nom au pluriel
		'name'                => _x( 'Crypts of Concept', 'ampp'),
		// Le nom au singulier
		'singular_name'       => _x( 'Crypt', 'ampp'),
		// Le libellé affiché dans le menu
		'menu_name'           => __( 'Crypt'),
		// Les différents libellés de l'administration
		'all_items'           => __( 'Crypts of Concept'),
		'view_item'           => __( ''),
		'add_new_item'        => __( 'Ajouter un Crypt'),
		'add_new'             => __( 'Ajouter'),
		'edit_item'           => __( 'Editer'),
		'update_item'         => __( 'Modifier'),
		'search_items'        => __( 'Rechercher'),
		'not_found'           => __( 'Aucun Crypt of Concept'),
		'not_found_in_trash'  => __( 'Non trouvée dans la corbeille'),
	);
	
	$args = array(
		'label'         => __( 'Crypts of Concept'),
		'description'   => __( 'Tous les Crypts'),
		'labels'        => $labels,
		'supports'      => array( 'title', 'custom-fields', ),
		'show_in_rest' 	=> true,
		'hierarchical'  => false,
		'public'        => true,
		'has_archive'   => true,
		'show_in_menu'  =>	'edit.php?post_type=work',
		'rewrite'			  => array( 'slug' => 'crypt'),

	);
	
	register_post_type( 'crypt', $args );
}

function ampp_columns_managing_crypt( $columns ){
	$columns = array(
		'cb' 							 => $columns['cb'],
		'illustration'  	 => 'Illustration',
		'title' 					 => 'Titre',
		);
	return $columns;
}

function ampp_custom_column_crypt( $column, $post_id ){

	if ( 'illustration' == $column ){
		$illustration = get_field('image_crypt', $post_id );

		if ( !$illustration ){
			echo "<img src='". IMG_URL ."placeholder.jpg' alt='". get_the_title($post_id) ."' width='80' height='80' />";
		}else{
			echo wp_get_attachment_image( $illustration, array(80,80) );
		}
	}

}