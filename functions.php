<?php

/* css & js url constants */
define('CSS_URL', get_template_directory_uri() . '/assets/css/');
define('JS_URL', get_template_directory_uri() . '/assets/js/');
define('IMG_URL', get_template_directory_uri() . '/assets/images/');
define('THEME_URL', get_template_directory_uri() . '/');
define('THEME_DIR', get_template_directory() . '/');
// define('WP_POST_REVISIONS', false);
/* Load all CPTs */
require('cpt/work.php');
require('cpt/reference.php');
require('cpt/client.php');
require('cpt/crypt.php');
require('require/theme_customiser.php');
require( "require/ajax-crypts.php" );

function make_phone_callable($phone){
	$phone = str_replace(array(' ','(',')'),array('','',''), $phone);
	return $phone;
}

add_action( 'wp_print_scripts', 'disable_autosave' );
function disable_autosave() {
	wp_deregister_script('autosave');
}

function remove_quick_edit( $actions, $post ) { 
	if ( $post->post_type == 'work' || $post->post_type == 'reference' ) {
		unset($actions['inline hide-if-no-js']);
		return $actions;
	}
}
add_filter('post_row_actions','remove_quick_edit',10, 2);

function to_slug($string){
    return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string), '-'));
}

function get_hashtags( $post_id ){
	$classname = array();
	$terms = get_the_terms(  $post_id, 'hashtag' );

	if ( $terms ){
		foreach ( $terms as $term ){
			$classname[] = $term->slug;
		}
	}
	return implode(" ", $classname);
}

function make_2lines($nom){
	$noms = explode(" ", $nom);
	$nom_2l = array();
	$length = count( $noms );
	if ( $length ){
		foreach ( $noms as $k => $n ){
			if ( $k != 0 ){
				$nom_2l[] = $n;
			}
		}
	}
	
	$nom = $noms[0].'<br>'. implode(' ', $nom_2l) ;

	return $nom;
}

function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyC0GV2a4o9JfBJof2rd0efbGVQTKF-XT0s');
}

add_action('acf/init', 'my_acf_init');

add_filter( 'shortcode_atts_wpcf7', 'sc_agent_email_attr', 10, 3 ); 
function sc_agent_email_attr( $out, $pairs, $atts ) {
	$rdv_attr = 'agent';

	if ( isset( $atts[$rdv_attr] ) ) {
		$out[$rdv_attr] = $atts[$rdv_attr];
	}

	return $out;
}
add_filter( 'shortcode_atts_wpcf7', 'sc_admin_cc_email_attr', 10, 3 ); 
function sc_admin_cc_email_attr( $out, $pairs, $atts ) {
	$rdv_attr = 'admin_cc';

	if ( isset( $atts[$rdv_attr] ) ) {
		$out[$rdv_attr] = $atts[$rdv_attr];
	}

	return $out;
}

add_action( 'wpcf7_before_send_mail', 'action_wpcf7_before_send_mail', 10, 1 ); 
function action_wpcf7_before_send_mail( $contact_form ){
	$submission = WPCF7_Submission::get_instance();

	if ( $submission ){
		$agent = $_POST['agent'];
		$reply_to = $_POST['email'];
		$cc = $_POST['admin_cc'];
		$client = $submission->get_posted_data();
		$client = $client['email'];
	}
	$recipient = $agent;
	if( $recipient) {
		$mail = $contact_form->prop( 'mail' ); 
		$mail['recipient'] = $recipient;
		if ( $reply_to ){
			$mail['additional_headers'] = 'Cc:'.$cc.','.$client;
		}
		$contact_form->set_properties(array('mail'=>$mail));
	}
}

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
  if (in_array('current-menu-item', $classes) ){
    $classes[] = 'active ';
  }
  return $classes;
}

add_filter( 'wp_mail_from_name', 'wpb_sender_name' );
add_filter('wp_mail_from', 'my_wp_mail_from');

function wpb_sender_name( $original_email_from ) {
    return 'AMPP';
}

function my_wp_mail_from($email) {
return 'contact@ampp.be';
}

function get_related_works( $pid, $client ){
	$metaquery = array(
		array(
			'key' 	=> 'le_client',
			'value' => $client,
			'compare'=> 'LIKE',
			));
	$args = array(
		'post_type' => 'work',
		'meta_query'=> $metaquery,
		'post__not_in' 				=> array($pid),
		);
	$r_works = get_posts( $args );

	$output = array();
	if ( is_array($r_works) ):
		$out = '';
			foreach ($r_works as $k => $re):
				$id = $re->ID;
		    $client = get_field('le_client', $id); 
		      if ( is_array( $client) ){
		        $client = $client[0]->post_title;
		      }else{
		        $client = '';
		      }
		    $realisation = get_the_title($id);

		    $illustration = get_field('lillustration', $id );
		    $img = "<img src='". IMG_URL ."placeholder.jpg' alt='". get_the_title($id) ."' width='310' height='317' />";
		    if ( $illustration ){
		      $img = wp_get_attachment_image( $illustration,'full', array( 'alt' => get_the_title($id) ) );
		    }
		    $bg_color = get_field('couleur_en_hover_survol', $id) ? get_field('couleur_en_hover_survol', $id) : '#ffffff';
		    $classname  = get_hashtags( $id );
		    $output[$k]['classname'] = $classname;
		    $output[$k]['bgcolor']   = $bg_color.'cc';
		    $output[$k]['img']       = $img;
	
	endforeach; endif;
	
	wp_reset_postdata();
	return $output;
}

function get_all_related_ids($pid, $client){
	$metaquery = array(
		array(
			'key' 	=> 'le_client',
			'value' => $client,
			'compare'=> 'LIKE',
			));
	$args = array(
		'post_type' => 'work',
		'meta_query'=> $metaquery,
		'post__not_in' 				=> array($pid),
		);
	$r_works = query_posts( $args );

	$ids = array();
	if ( is_array($r_works) ):
		foreach ($r_works as $k => $re):
			$ids[] = $re->ID;
		endforeach;
		wp_reset_query();
	endif;

	return $ids;
}
