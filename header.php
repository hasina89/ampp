<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>
	<?php wp_title('|', true, 'right').'|'. bloginfo('name'); ?>
</title>
  
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<meta name="keywords"  content="fullpage,scrolloverflow,inside,overflow,scroll,jquery,demo,screen,fullscreen,backgrounds,full-screen" />
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9" />
<meta name="google-site-verification" content="yxEbdNlvlle9P8AuzzANZ2ngQq7ECYPx3XLJAUyQ57A" />
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC1jfbq8wQ2-91jfY7pcXMih5BLxdd2j_M"></script> -->
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<meta property="og:url"                content="<?= home_url() ?>" />
<meta property="og:type"               content="article" />
<meta property="og:title"              content="AMPP" />
<meta property="og:description"        content="AMPP is a design studio based in Brussels, Belgium." />
<meta property="og:image"              content="<?= IMG_URL . 'logo.png' ?>" />

<?php 
	wp_head(); 
?>
</head>
<?php 
	if( is_page('our-works') ){
		$body_class = 'template-page-work';
	}else{
		$body_class = 'template-page-about';
	}
	  
?>
<body <?php  body_class( $body_class ) ?>">
    	<div id="wrapper">
        	<?php require( "templates/header-content.php" );?>