<?php 
	/*
	Template name: about-page
	*/

	get_header();

	require( THEME_DIR."/require/video-about.php" );
?>

<section class="section section2 sec-ctc-about" data-section="about-content" id="first">
	<div class="wrapper">
		<div class="row">
			<div class="col left">
				<div class="text" data-aos="fade-up"  data-aos-duration="600">
					<?= get_field('texte_a_propos') ?>
				</div>
				<a href="<?= site_url('/contact/'); ?>" class="link" title="CONTACT US" data-aos="fade-up"  data-aos-duration="1000">
					<span><?= get_field('label_bouton'); ?></span>
				</a>
			</div>
			<div class="col right" data-aos="fade-up"  data-aos-duration="1600">
				<div class="img">
            <?= wp_get_attachment_image( get_field('image_apropos'), 'rightimage-about', ["alt"=>"AMPP"] ); ?>
        </div>
			</div>
		</div>		
	</div>
</section>

<!-- Filtre About-page -->
<section class="section section3 fp-auto-height sec-filter" data-section="filter-work">
	<div class="wrapper">
		<h2 data-aos="fade-up"  data-aos-duration="600">Capabilities.</h2>
			
			<?php 
				require( THEME_DIR."/require/menu-filter-work.php" );
			?>
		</div>
</section>
<section class="section section4 fp-auto-height sec-filter" data-section="filter-work">
	<div class="wrapper">
<?php
				require( THEME_DIR."/require/work-loop.php" );
				require( THEME_DIR."/require/work-in-about.php" );
			?>

			<div class="blcLink">
				<a href="<?= site_url('/contact/'); ?>" class="link" title="CONTACT US" data-aos="fade-up"  data-aos-duration="2000">
					<span><?= strtoupper( get_field('label_bouton') ); ?></span>
				</a>
			</div>

	</div>
</section>

<section class="section section5 fp-auto-height sec-ref last-about">
	<div class="wrapper">
		<div class="content" data-aos="fade-up"  data-aos-duration="800">
			<h2>References.</h2>

			<?php 
				require( THEME_DIR."/require/reference-loop.php" ); 
				require( THEME_DIR."/require/reference-in-about.php" ); 
			?>
		</div>
	</div>
</section>

<?php get_footer(); ?>