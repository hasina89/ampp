<?php
	/*
	Template name: works-page
	*/

	get_header(); 
?>

<section id="first" class="section section1 fp-auto-height filter-work">
	<div class="aos">
		<div class="wrapper workFilter filterValueWork1"  >
			<h2>Filter our work.</h2>
			<?php require( THEME_DIR."/require/menu-filter-work.php" ); ?>
		</div>
	</div>
</section>

<section class="section section2 fp-auto-height sec-item-work">
	<div class="content-item-work">
		<div class="wrapper filterValue filterValueWork2">
			<?php require( THEME_DIR."/require/work-in-list.php" ); ?>
		</div>
	</div>
</section>

<section class="section section3 crypt">
	<div class="wrapper">
			<h2 data-aos="fade-up" data-aos-duration="600">
				<?= get_field('titre_de_la_section') ?>
			</h2>

		  <p data-aos="fade-up" data-aos-duration="800"><?= get_field('chapeau_de_la_section') ?></p>

		  <?php 
		  	$cr = wp_count_posts('crypt')->publish; 
		  	// $cr = $cr->publish;
		  ?>

		  <?php if ( $cr > 0 ): ?>
			  <div class="imgCrypt" data-aos="fade-up" data-aos-duration="1200" id="crypt-container">
			  	<?php include THEME_DIR."/require/work-crypts.php"; ?>
			  </div>
			  <?php if ( $cr > 4 ): ?>
				  <a class="link" title="SEE MORE" data-aos="fade-up" data-aos-duration="1800" id="load-more">
				  	<span><?= strtoupper( get_field('label_du_bouton') ); ?></span>
				  </a>
				<?php endif; ?>
			<?php else: ?>
				<div data-aos="fade-up" data-aos-duration="1200">
			  	<h3 style="text-align:center;margin-top:2em">-- No Crypts of Concepts to show --</h3>
			  </div>
			<?php endif; ?>
	</div>
</section>
<?php get_footer(); ?>