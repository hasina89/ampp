<?php 
	/* 
		Template name: home 
	*/
get_header(); ?>

<section id="first" class="section sec-home section1 sec1-home" data-section="about">          
    <div class="blc-about">
        <div class="wrapper">
            <h2 data-aos="fade-up"  data-aos-duration="1000"  data-aos-easing="ease-in-out" data-aos-delay="500">
            	<?= get_field('texte_about') ?>
            </h2>
            <a href="<?= home_url('/about/') ?>" class="link" title="Read more about us" data-aos="fade-up"  data-aos-duration="2000" data-aos-delay="500" data-aos-easing="ease-in-out">
            	<span><?= get_field('texte_bouton_a_propos') ?></span>
            </a>
        </div>
    </div>
</section>

<?php 	
	require( THEME_DIR."/require/work-loop.php" );

	require( THEME_DIR."/require/work-in-home.php" );
?>

<section class="section section3 sec-home sec3-home before-foot-home" data-section="who-we-are">
    <div class="qui-sommes-nous">
        <div class="wrapper">
            <!-- <h2 data-aos="fade-up"  data-aos-duration="800">
            	<?= get_field('titre_section_principale'); ?>
            </h2> -->
            <div class="row clr">
                <div class="col" data-aos="fade-right"  data-aos-duration="800">
                    <div class="txt">
                        <p>
                        	<span>
                        		<?= get_field('titre_section_post_work'); ?>
                        	</span>
                        </p>
                        <p>
                        		<?= get_field('contenu_section_post_work'); ?>
                        </p>
                        <!-- <a href="<?= home_url('/about/'); ?>" class="link" title="Who we are"><span>WHO WE ARE</span></a> -->
                        </div>
                    </div>
                    <div class="col" data-aos="fade-left"  data-aos-duration="800">
                        <div class="img">
                            <?= wp_get_attachment_image( get_field('image_section_post_work'), 'rightimage-home', ["alt"=>"AMPP"] ); ?>
                        </div>
                 </div>     
            </div>           
        </div>
    </div>
</section>
<?php get_footer(); ?>