<?php 
	/**
	Template name: contact-page
	**/

	get_header();

	$adresse = get_field('adresse_map'); 
	$ville   = get_field('ville');
	$urlencoded = urlencode($adresse.$ville);
?>

<section id="first" class="section">
	<div class="wrapper blocProfil">

		<?php require( THEME_DIR."/require/contact-profile.php" ); ?>

	</div>
</section>

<section class="section section2 sec-find-us">
	<div class="wrapper workFilter blocFind" data-aos="fade-up" data-aos-duration="800">
		<h2>Find us.</h2>
		<div class="adr">
			<p><?= get_field('adresse_map') ?><br><?= get_field('zip_code') ?> - <?= get_field('ville') ?></p>
			<p><a href="#" title="Go to the destination">Go to the destination&nbsp;&nbsp;></a></p>
		</div>
	</div>
		<div class="wrapper blocMap" data-aos="fade-up" data-aos-duration="1200">
						<div class="map">
							<iframe src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=<?= $urlencoded ?>+(AMPP)&amp;t=&amp;z=16&amp;ie=UTF8&amp;iwloc=B&amp;output=embed" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>

						</div>
					</div>
</section>

<?php get_footer(); ?>