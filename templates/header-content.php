<!--<div id="preloder">
<div class="loader"></div>
<div class="loaderLogo">
            <img src="<?= IMG_URL ?>logo.png" alt="AMPP">
        </div>
</div>-->
<div id="butter" <?php if ( is_single() ) echo "class='Detail'"; ?> <?php if ( is_front_page() ) echo "class='home'"; ?>>
    <?php 
                                        
        $titre_h1 = get_field('titre_h1');                    

        if ( get_field( 'image_h1' ) ){
            $image_h1 = wp_get_attachment_image_src( get_field( 'image_h1' ), 'workimage-home', ["alt"=>"AMPP"] );
            $image_h1 = $image_h1[0];
        }else{
            $image_h1 = IMG_URL . "img1.jpg";
        }

        $classname = '';
        
        if( is_page( "our-works" ) || is_page( "contact" ) ){
            $classname = ' workLeft';
        }elseif( is_page( "legal-notices" ) ){
            $classname = ' workType';
        }              
    
    if( !is_home() && !is_front_page() && !is_page( 'our-works')){
    ?>
        <section class="section sec-home section0 page" <?php if( !is_singular( array('work','crypt') ) ): ?> style="background-image:url(<?= $image_h1 ?>);background-size:cover; background-position: center" <?php endif; ?>>
            <div class="content-top">
            	<div class="headerTop"  data-aos="fade-down"  data-aos-duration="1000"  data-aos-easing="ease-in-out">
                    <a href="<?php echo site_url() ?>" class="logo">
                    <?php
                        if ( get_theme_mod( 'upload_logo' ) > 0 ) { 
                            echo wp_get_attachment_image( get_theme_mod( 'upload_logo' ), 'full', ["alt"=>"AMPP"] ); 
                        }
                    ?>
                    </a>
                    <div class="right">
                        <div class="wrapMenuMobile"><div class="menuMobile"><div></div></div></div>
                        <?php
                            wp_nav_menu( array(
                                'theme_location' => 'top-menu',
                                'container' => 'nav',
                                'container_class' => 'menu'
                            ) );
                        ?>
                        <div class="social">
                            <ul>
                                <li class="fb"><a href="https://www.facebook.com/AMPP.be" target="_blank"><img src="<?= IMG_URL ?>facebook.svg" title="Partagez-nous sur facebook"></a></li>
                                <li class="linkedin"><a 
                                      href="https://www.linkedin.com/company/ampp.be/"
                                      data-show-screen-name="false"
                                      data-show-count="false" target="_blank">
                                      <img src="<?= IMG_URL ?>linkden.svg" title="Suivez-nous sur linkden"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="blocSlider"> 
                    
                    <?php       
                        $couleur_titre = get_field('couleur_titre') ? get_field('couleur_titre') : 'white';
                    ?>
                        <style type="text/css">
                            .section .slider .h1{
                                color: <?= $couleur_titre ?> !important;
                            }
                        </style>
                    <?php

                        if ( is_single() ):    
                            $couleur_menu = get_field('couleur_du_menu') ? get_field('couleur_du_menu') : 'white';                
                            $ban = '';
                            if ( get_field( 'image_a_mettre_en_avant' ) ){
                                $ban = wp_get_attachment_image( get_field( 'image_a_mettre_en_avant' ), 'full' );
                            }elseif( get_field( 'image_a_mettre_en_avant_crypt' ) ){
                                $ban = wp_get_attachment_image( get_field( 'image_a_mettre_en_avant_crypt' ), 'full' );
                            }else{
                                $ban = "<img src='".IMG_URL . "banner-work.jpg' />";
                            }
                        ?>
                            <div class="slider workType">
                                <?= $ban ?>
                            </div>
                            <!-- <a href="<?= home_url('/our-works/') ?>" class="closeDetail">
                                <img src="<?= IMG_URL ?>close.svg" alt="">
                            </a>-->
                            <style type="text/css">
                                .Detail .menu a{
                                    color: <?= $couleur_menu ?> !important;
                                }
                            </style>
                        
                    <?php elseif ( is_page( "legal-notices" ) ) :?>
                        <div class="slider workType" data-aos="fade-up"  data-aos-duration="1000"  data-aos-easing="ease-in-out">
                            <div class="content">
                                <h1 class="h1">
                                    <?= $titre_h1; ?>.
                                </h1>
                            </div>
                        </div>
                    <?php elseif ( is_page( "about" ) ) :?>
                        <div class="slider" data-aos="fade-up"  data-aos-duration="1000"  data-aos-easing="ease-in-out">
                            <div class="content">
                                <h1 class="h1">
                                    <?= $titre_h1; ?>.
                                </h1>
                            </div>
                        </div>
                     <?php elseif ( is_page( "contact" ) ) :?>
                        <div class="slider workLeft" data-aos="fade-up"  data-aos-duration="1000"  data-aos-easing="ease-in-out" data-aos-delay="500">
                            <div class="content">
                                <h1 class="h1">
                                    <?= $titre_h1; ?>.
                                </h1>
                            </div>
                        </div>
                    <?php elseif ( is_page( "our-works" ) ) :?>
                        <!-- <div class="slider workLeft" data-aos="fade-up"  data-aos-duration="1000"  data-aos-easing="ease-in-out" data-aos-delay="500">
                            <div class="content">
                                <h1 class="h1">
                                    <?= $titre_h1; ?>.
                                </h1>
                            </div>
                        </div> -->
                    <?php else: 
                    ?>
                        <div class="slider<?= $classname ?>" data-aos="fade-up"  data-aos-duration="1000"  data-aos-easing="ease-in-out" data-aos-delay="500">
                            <div class="content">
                                <h1 class="h1" >
                                    <span><?= $titre_h1; ?>.</span>
                                </h1>
                            </div>
                        </div>
                    <?php endif; ?>
                    <a class="scrollTop"></a>
                </div>
            </div>
        </section>
    <?php 
    }elseif( is_home() || is_front_page() ){ 
    ?>
        <section class="section sec-home section0">
            <?php
                $works = get_posts('post_type=work&posts_per_page=-1');
                wp_reset_postdata();
            ?>
            <div id="bg_slide">
                <?php 
                    if( is_array( $works ) ){ 
                        foreach( $works as $work ){
                            if ( get_field( 'image_a_mettre_en_avant', $work->ID ) ){
                                $image_h1 = wp_get_attachment_image_src( get_field( 'image_a_mettre_en_avant', $work->ID ), 'workimage-home', ["alt"=>"AMPP"] );
                                $image_h1 = $image_h1[0];
                            }else{
                                $image_h1 = IMG_URL . "placeholder.png";
                            }
                ?>
                        <div style="background-image:url(<?= $image_h1 ?>);background-size:cover; background-position: center"><img src="<?= $image_h1 ?>" style="opacity: 0;visibility: hidden;"></div>
                <?php } 
                    } 
                ?>
            </div>
            <div class="content-top">
                <div class="headerTop"  data-aos="fade-down"  data-aos-duration="1000"  data-aos-easing="ease-in-out">
                    <a href="<?php echo site_url() ?>" class="logo">
                    <?php
                        if ( get_theme_mod( 'upload_logo' ) > 0 ) { 
                            echo wp_get_attachment_image( get_theme_mod( 'upload_logo' ), 'full', ["alt"=>"AMPP"] ); 
                        }
                    ?>
                    </a>
                    <div class="right">
                        <div class="wrapMenuMobile"><div class="menuMobile"><div></div></div></div>
                        <?php
                            wp_nav_menu( array(
                                'theme_location' => 'top-menu',
                                'container' => 'nav',
                                'container_class' => 'menu'
                            ) );
                        ?>
                        <div class="social">
                            <ul>
                                <li class="fb"><a href="https://www.facebook.com/AMPP.be" target="_blank"><img src="<?= IMG_URL ?>facebook.svg" title="Partagez-nous sur facebook"></a></li>
                                <li class="linkedin">
                                    <a 
                                      href="https://www.linkedin.com/company/ampp.be/"
                                      data-show-screen-name="false"
                                      data-show-count="false" target="_blank">
                                        <img src="<?= IMG_URL ?>linkden.svg" title="Suivez-nous sur linkden">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="blocSlider"> 
                    
                    <?php       
                        $couleur_titre = get_field('couleur_titre') ? get_field('couleur_titre') : 'white';
                    ?>
                        <style type="text/css">
                            .section .slider .h1,
                            .btnArrow span{
                                color: <?= $couleur_titre ?> !important;
                            }
                        </style>   
                        <div class="slider" data-aos="fade-up"  data-aos-duration="1000"  data-aos-easing="ease-in-out" data-aos-delay="500">
                            <div class="content">
                                <div class="bgH1">
                                    <h1 class="h1"><?= $titre_h1; ?></h1>
                                </div>
                            </div>
                        </div>
                    <a class="scrollTop"></a>
                </div>
            </div>
        </section>
    <?php 
    }elseif( is_page('our-works') ){ ?>
        <section class="section sec-home section0 section-work page">
            <div class="content-top">
                <div class="headerTop">
                    <a href="<?php echo site_url() ?>" class="logo">
                    <?php
                        if ( get_theme_mod( 'upload_logo' ) > 0 ) { 
                            echo wp_get_attachment_image( get_theme_mod( 'upload_logo' ), 'full', ["alt"=>"AMPP"] ); 
                        }
                    ?>
                    </a>
                    <div class="right">
                        <div class="wrapMenuMobile"><div class="menuMobile"><div></div></div></div>
                        <?php
                            wp_nav_menu( array(
                                'theme_location' => 'top-menu',
                                'container' => 'nav',
                                'container_class' => 'menu'
                            ) );
                        ?>
                        <div class="social">
                            <ul>
                                <li class="fb"><a href="https://www.facebook.com/AMPP.be" target="_blank"><img src="<?= IMG_URL ?>facebook.svg" title="Partagez-nous sur facebook"></a></li>
                                <li class="linkedin"><a 
                                      href="https://www.linkedin.com/company/ampp.be/"
                                      data-show-screen-name="false"
                                      data-show-count="false" target="_blank">
                                      <img src="<?= IMG_URL ?>linkden.svg" title="Suivez-nous sur linkden"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php }
