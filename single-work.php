<?php 
	get_header();	
			if (have_posts() ):
						while( have_posts() ): the_post();
	?>

		<section id="first" class="section section1 claiming fp-auto-height">
			<div class="wrapper">
				<h2 data-aos="fade-up" data-aos-duration="600"><?= get_the_title() ?></h2>
				<?php
					

						$ps = get_field('les_paragraphes');
						if ( is_array($ps) ): 
							$left_p = $ps[0]['un_paragraphe'];
							if ( isset( $ps[1]) )
								$right_p = $ps[1]['un_paragraphe'];
						endif;
						

						$facebook_url 	= 'https://www.facebook.com/sharer/sharer.php?u='.get_permalink();
						$linkedin_url		= 'https://www.linkedin.com/shareArticle?mini=true&url='. get_permalink() .'&title='. sanitize_title( get_the_title() ).'&summary='. sanitize_title( $client ).'&source=LinkedIn';
						$instagram_url  = 'https://www.instagram.com/?url='.get_permalink();
						$twitter_url    = 'https://twitter.com/intent/tweet?text='. get_the_title().'&url='.get_permalink();
				?>
				<div class="claimingText <?php if ( !$left_p) echo 'tokony-tsis-padding' ?>">
					<?php if ( $left_p ): ?>
						<div class="claiming1" data-aos="fade-up" data-aos-duration="1000">
							<?= $left_p ?>						
						</div>
					<?php endif; ?>
					<?php if ( $right_p ): ?>
						<div class="claiming2" data-aos="fade-up" data-aos-duration="1200">
					<?php endif; ?>
						<?= $right_p ?>
							<ul class="listeClaiming">
							<?php

								$r_ids = get_the_terms($post->ID, 'hashtag');
								$hashslug = array();
								$hashname = array();
								foreach ($r_ids as $rid ){ 
								?>
								
								<?php
										?>
										<li>#<?= $rid->name; ?></li>
										<?php
									}
								?>
								</ul>
					<?php if ( $right_p ): ?>	
						</div>
					<?php endif; ?>
				</div>
			</div>
		</section>
		<section class="section fp-auto-height">
			<div class="wrapper">
				<div id="vue" class="imgClaiming listItemA gridA">
					<?php
								$gals = get_field('galeries_dimages');

								if ( is_array( $gals) ):
									foreach ($gals as $gal ):
					?>
								<a href="javascript: void(0)" class="element-item">
									<figure>
										<!-- <span class="color" style="background:<?= $gal['couleur_en_hover_au_survol'] ?>"></span> -->
										<?=  
											wp_get_attachment_image( $gal['image_galerie'], 'full' );
										?>
									</figure>
								</a>
					<?php	
									endforeach;
								endif;	

								$gal_video = get_field('galerie_videos');
								if( is_array( $gal_video ) ){
									foreach( $gal_video as $vid ){
					?>
										<a href="javascript: void(0)" class="element-item">
											<video controls width="250" style="display: block;width: 100%;">
												<source src="<?= $vid['video_galerie']['url'] ?>" type="video/<?= $vid['video_galerie']['subtype'] ?>">
											</video>
										</a>
					<?php
									}
								}		
					?>
					</div>
					<div class="rsBtn" data-aos="fade-up" data-aos-duration="800">
						<a href="<?= home_url('/contact/') ?>" class="link" title="Contact us"><span>Contact us</span></a>
						
						<ul class="rs">
							<li>Partager sur</li>
							<li><a href="<?= $facebook_url ?>" target="_blank"><img src="<?= IMG_URL ?>facebook.svg" alt=""></a></li>
							<li><a href="<?= $instagram_url ?>" target="_blank"><img src="<?= IMG_URL ?>instagram.svg" alt=""></a></li>
							<li><a href="<?= $linkedin_url ?>" target="_blank"><img src="<?= IMG_URL ?>linkden.svg" alt=""></a></li>
							<li><a href="<?= $twitter_url ?>" target="_blank"><img src="<?= IMG_URL ?>twitter.svg" alt=""></a></li>
						</ul>
					</div>
			</div>
		</section>

<?php 
		endwhile;
						wp_reset_postdata();
					endif; 
	get_footer(); ?>