<?php
	get_header();

	if (have_posts()) {
		while (have_posts()){
			the_post();
?>
			<section id="first" class="section section0">
				<div class="wrapper" data-aos="fade-up"  data-aos-duration="1000"  data-aos-easing="ease-in-out" data-aos-delay="500">
				<?php the_content(); ?>
				</div>
			</section>
<?php
		}
	}

get_footer(); ?>