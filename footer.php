<?php 
     $footer_logo = get_theme_mod( 'footer_logo' ) > 0 ? wp_get_attachment_image( get_theme_mod( 'footer_logo' ), 'footer-logo' ) : IMG_URL.'logo.png';
     $title_2 = strtoupper( get_theme_mod( 'title_col2', 'NEW BUSINESS INQUIRIES') );
     $email_2 = get_theme_mod('email_col2', 'hello@ampp.be'); 
     $phone_2 = get_theme_mod('phone_col2', '+32 (0)2 524 23 22');

     $title_3 =  strtoupper( get_theme_mod( 'title_col3', 'JOBS & INTERNSHIPS') );
     $email_3 = get_theme_mod('email_col3', 'work@ampp.be');
?>
<section class="section sec-home sec-foot section6 fp-auto-height <?php if ( !is_front_page() && !is_home() ) echo ' page'; ?>">         
	<div class="footer clr" data-aos="fade-up"  data-aos-duration="1000"  data-aos-easing="ease-in-out" data-aos-delay="500">
          <div class="wrapper">
               <div class="cont-footer clr">
          		<div class="col">
                         <a href="<?php echo site_url() ?>" class="logo-ftr">
                              <?= $footer_logo ?>
                         </a>        
                    </div> 
                    <div class="col">
                         <div class="titre">
                              <?= $title_2  ?>
                         </div>
                         <ul>
                              <li><a href="mailto:lau@ampp.be,jp@ampp.be" title="<?= $email_2 ?>"><?= $email_2 ?></a></li>
                              <li><a href="tel:<?= make_phone_callable($phone_2) ?>" title="<?= $phone_2 ?>"><?= $phone_2 ?></a></li>
                         </ul>
                    </div> 
                    <div class="col">
                         <div class="titre">
                             <?= $title_3  ?>
                         </div>
                         <ul>
                              <li><a href="mailto:lau@ampp.be,jp@ampp.be" title="<?= $email_3 ?>"><?= $email_3 ?></a></li>
                         </ul>
                    </div>
                    <div class="col-right">
                         <?php 
                              // wp_nav_menu( array(
                              //      'theme_location' => 'bottom-menu',
                              //      'container' => '',
                              //      'container_class' => '',
                              //      'menu_class'=>'',
                              // ) ); 
                         ?>
                         <div class="social d-flex">
                            <ul>
                                   
                                <li class="fb"><a href="https://www.facebook.com/AMPP.be" target="_blank"><img src="<?= IMG_URL ?>facebook.svg" title="Partagez-nous sur facebook"></a></li>
                                <li class="linkedin"><a 
                                      href="https://www.linkedin.com/company/ampp.be/"
                                      data-show-screen-name="false"
                                      data-show-count="false" target="_blank">
                                      <img src="<?= IMG_URL ?>linkden.svg" title="Suivez-nous sur linkden"></a></li>
                            </ul>
                        </div>
                        
                         <?php 
                              wp_nav_menu( array(
                                   'theme_location' => 'bottom-menu',
                                   'container' => '',
                                   'container_class' => '',
                                   'menu_class'=>'',
                              ) ); 
                         ?>
                         
                    </div>                     
               </div>
     	</div>
     </div>      
 </section>
 </div>
 </div> <!-- opened in header.php -->
<?php wp_footer(); ?>
</body>
</html>
